[TOC]

# aisa-backups

## Настройка регулярных бэкапов

1. Установить `python3-venv`

```sh
apt-get install python3-venv
```

2. Создать виртуальное окружение

```sh
python3 -m venv .venv
```

3. Инициализировать виртуальное окружение

```sh
source ./.venv/bin/activate
```

4. Установить зависимости

```sh
pip install  -r requirements.txt
```

5. Создать новую cron-запись

```sh
# Открыть crontab
crontab -e

# Добавить запись
/home/${username}/projects/aisa-backuper/.venv/bin/python /home/${username}/projects/aisa-backuper/backup.py --timezone='Asia/Vladivostok' --container='${container}' --dbname='${dbname}' --dbuser='${dbuser}' --dbpath='/home/${username}/projects/aisa-backuper/tmp' --bucket='${bucket}'
```

| Переменная | Описание | Пример |
|---|---|---|
| `username` | Имя текущего пользователя операционной системы | `kontingent` |
| `container` | Имя контейнера базы данных | `kontingent_postgres` | 
| `dbname` | Имя базы данных | `kontingent` |
| `dbuser` | Имя пользователя базы данных | `kontingent` |
| `bucket` | Названия бакета для баз данных | `kontingent-backups` |

## Разовый запуск бэкапа

```sh
/home/${username}/projects/aisa-backuper/.venv/bin/python /home/${username}/projects/aisa-backuper/backup.py --timezone='Asia/Vladivostok' --container='${container}' --dbname='${dbname}' --dbuser='${dbuser}' --dbpath='/home/${username}/projects/aisa-backuper/tmp' --bucket='${bucket}'
```