"""
Backup PostgreSQL database to Yandex Object Storage, that has S3 compatible
API.
"""
import os
import pytz
import logging
import argparse
from pathlib import Path
from datetime import datetime
from functools import reduce
from tzlocal import get_localzone
from hurry.filesize import size

from termcolor import colored
import boto3

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', filename='backups.log', level=logging.DEBUG)

LOCAL_TIME_ZONE = get_localzone()

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--timezone', type=str)
    parser.add_argument('--container', type=str)
    parser.add_argument('--dbname', type=str)
    parser.add_argument('--dbuser', type=str)
    parser.add_argument('--dbpath', type=str)
    parser.add_argument('--bucket', type=str)

    args = parser.parse_args()

    missed_args = list(map(lambda el: el[0], filter(lambda el: el[1] is None, vars(args).items())))

    if len(missed_args) > 0:
        print(colored("Missed arguments: {0}. You must specify them.".format(", ".join(missed_args)), "red"))
        exit()
    else:
        return args


def say_hello():
    print(colored("\nHi! This tool will dump PostgreSQL database, compress \n"
        "and then send to Yandex Object Storage.\n", "cyan"))

def get_backup_path(target_tz, dbname, dbpath):
    date = LOCAL_TIME_ZONE.localize(datetime.now()).astimezone(pytz.timezone(target_tz)).strftime("%Y-%m-%d_%H-%M-%S")
    filename = "{name}_{date}.gz".format(name=dbname, date=date)
    return os.path.join(dbpath, filename)

def dump_database(container, dbname, dbuser, path):
    print("\U0001F4E6 Preparing database backup")
    os.system("docker exec {container} pg_dump {dbname} -U {dbuser} -Fc > {path}".format(
        container=container,
        dbname=dbname,
        dbuser=dbuser,
        path=path
    ))

def get_s3_instance():
    session = boto3.session.Session()
    return session.client(
        service_name='s3',
        endpoint_url='https://storage.yandexcloud.net'
    )

def upload_dump_to_s3(path, bucket):
    print(path)
    if Path(path).exists():
        print("\U0001F4C2 Upload to Yandex Object Storage")
        get_s3_instance().upload_file(
            Filename=path,
            Bucket=bucket,
            Key=os.path.basename(path)
        )
        filesize = Path(path).stat().st_size
        logging.debug("Backup has been uploaded, {size}".format(
            size=size(filesize)
        ))
        print("\U0001f680 Uploaded, {size}".format(
            size=size(filesize)
        ))
    else:
        print(colored("\nBackup file does not exist!", "red"))
        exit()

def remove_temp_files(path):
    os.remove(path)
    print(colored("\nOk. Everything is done!\n", "green"))

if __name__ == "__main__":
    say_hello()
    args = parse_args()
    backup_path = get_backup_path(args.timezone, args.dbname, args.dbpath)
    dump_database(args.container, args.dbname, args.dbuser, backup_path)
    upload_dump_to_s3(backup_path, args.bucket)
    remove_temp_files(backup_path)